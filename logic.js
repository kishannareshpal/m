window.onload = function() {
    var config = {
        apiKey: "AIzaSyBsqI-AGvHpvdaDyvMQhSJJE9Pt01VgN5c",
        storageBucket: "examesproject.appspot.com"
    };

    firebase.initializeApp(config);
    var slectClasse = document.getElementById("slectClasse"),
        slectDisc = document.getElementById("slectDisc"),
        slectAno = document.getElementById("slectAno");
    for (var classe in classeObject) {
        slectClasse.options[slectClasse.options.length] = new Option(classe, classe)
    }
    slectClasse.onchange = function() {
        slectDisc.length = 1;
        slectAno.length = 1;
        if (this.selectedIndex < 1) return;
        for (var disciplina in classeObject[this.value]) {
            slectDisc.options[slectDisc.options.length] = new Option(disciplina, disciplina)
        }
    };
    slectClasse.onchange();
    slectEpoca.onchange();
    slectDisc.onchange = function() {
        slectAno.length = 1;
        $("#guiaAnchor").addClass("disabled");
        $("#enunciadoAnchor").addClass("disabled");
        document.getElementById("loadingStuff").setAttribute('hidden', '');
        if (this.selectedIndex < 1) return;
        var anos = classeObject[slectClasse.value][this.value];
        for (var i = 0; i < anos.length; i++) {
            slectAno.options[slectAno.options.length] = new Option(anos[i], anos[i])
        }
    };
    var selectUniversidade = document.getElementById("uniSlectUni"),
        selectCadeira = document.getElementById("uniSlectCad"),
        selectAno = document.getElementById("uniSlectAno");
    for (var universidade in universidadeObject) {
        selectUniversidade.options[selectUniversidade.options.length] = new Option(universidade, universidade)
    }
    selectUniversidade.onchange = function() {
        selectCadeira.length = 1;
        selectAno.length = 1;
        if (this.selectedIndex < 1) return;
        for (var cadeira in universidadeObject[this.value]) {
            selectCadeira.options[selectCadeira.options.length] = new Option(cadeira, cadeira)
        }
    };
    selectUniversidade.onchange();
    selectCadeira.onchange = function() {
        selectAno.length = 1;
        $("#uniEnunciadoAnchor").addClass("disabled");
        $("#uniGuiaAnchor").addClass("disabled");
        document.getElementById("loadingStuffOnUniversidade").setAttribute('hidden', '');
        if (this.selectedIndex < 1) return;
        var anos = universidadeObject[selectUniversidade.value][this.value];
        for (var i = 0; i < anos.length; i++) {
            selectAno.options[selectAno.options.length] = new Option(anos[i], anos[i])
        }
    }
    selectCadeira.onchange();
};


function getSelectedNames() {
    var aClasse = document.getElementById("slectClasse").value;
    var aDisciplina = document.getElementById("slectDisc").value;
    var oAno = document.getElementById("slectAno").value;
    var aEpoca = document.getElementById("slectEpoca").value;
    var storage = firebase.storage();
    var storageRef = storage.ref();
    $("#guiaAnchor").addClass("disabled");
    $("#enunciadoAnchor").addClass("disabled");
    document.getElementById("loadingStuff").removeAttribute("hidden", "");
    var guiaReference = storageRef.child("Guias/" + aDisciplina + "/" + aClasse + "/" + oAno + "-" + aEpoca + ".pdf");
    guiaReference.getDownloadURL().then(function(urlGuia) {
        document.getElementById("loadingStuff").setAttribute("hidden", "");
        $("#guiaAnchor").removeClass("disabled");
        document.getElementById("guiaAnchor").href = urlGuia;
    }).catch(function(error) {
        $("#guiaAnchor").addClass("disabled");
        document.getElementById("loadingStuff").setAttribute("hidden", "");
    });
    var enunciadoReference = storageRef.child(aDisciplina + "/" + aClasse + "/" + oAno + "-" + aEpoca + ".pdf");
    enunciadoReference.getDownloadURL().then(function(urlEnunciado) {
        document.getElementById("loadingStuff").setAttribute("hidden", "");
        $("#enunciadoAnchor").removeClass("disabled");
        document.getElementById("enunciadoAnchor").href = urlEnunciado;
    }).catch(function(error) {
        $("#enunciadoAnchor").addClass("disabled");
        document.getElementById("loadingStuff").setAttribute("hidden", "");

    })
}

function getSelectedNamesSuperior() {
    var rarawUniversidade = document.getElementById("uniSlectUni").value;
    var rawUniversidade = document.getElementById("uniSlectUni").value.split("(");
    var splitRawUniv = rawUniversidade[1].split(")");
    var aUniversidade = splitRawUniv[0];
    var aCadeira = document.getElementById("uniSlectCad").value;
    var oAnoUniv = document.getElementById("uniSlectAno").value;
    var storageUniv = firebase.storage();
    var storageRefUniv = storageUniv.ref();
    $("#uniGuiaAnchor").addClass("disabled");
    $("#uniEnunciadoAnchor").addClass("disabled");
    document.getElementById("loadingStuffOnUniversidade").removeAttribute("hidden", "");
    if (aCadeira === "" || rarawUniversidade === "" || oAnoUniv === "") {
        alert("Por favor selecione todas as opções para disponibilizar o download")
    }
    var uniGuiaReference = storageRefUniv.child("Universidade/Guias/" + aUniversidade + "/" + aCadeira + "/" + aUniversidade + "-" + aCadeira + "-" + oAnoUniv + "-Guia.pdf");
    uniGuiaReference.getDownloadURL().then(function(urlUniGuia) {
        $("#uniGuiaAnchor").removeClass("disabled");
        document.getElementById("uniGuiaAnchor").href = urlUniGuia;
        document.getElementById("loadingStuffOnUniversidade").setAttribute("hidden", "");
    }).catch(function(error) {
        $("#uniGuiaAnchor").addClass("disabled");
        document.getElementById("loadingStuffOnUniversidade").setAttribute("hidden", "");
    });
    var uniEnunciadoReference = storageRefUniv.child("Universidade/" + aUniversidade + "/" + aCadeira + "/" + aUniversidade + "-" + aCadeira + "-" + oAnoUniv + ".pdf");
    uniEnunciadoReference.getDownloadURL().then(function(urlUniEnunciado) {
        $("#uniEnunciadoAnchor").removeClass("disabled");
        document.getElementById("uniEnunciadoAnchor").href = urlUniEnunciado;
        document.getElementById("loadingStuffOnUniversidade").setAttribute("hidden", "");
    }).catch(function(error) {
        $("#uniEnunciadoAnchor").addClass("disabled");
        document.getElementById("loadingStuffOnUniversidade").setAttribute("hidden", "");
    })
}
