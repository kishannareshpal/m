var granimInstance;

document.onreadystatechange = function () {
  var state = document.readyState;

  if (state == 'complete') {
      setTimeout(function(){
        // $("#loading").addClass("hidden");
        document.getElementById("app_loading").setAttribute("hidden", "");
        document.getElementById("app").removeAttribute("hidden", "");
        document.getElementById('body').classList.add('grad');


      },500);
  }
};

var bgcheck = document.getElementById('backgroundCheckbox');

if(document.cookie.indexOf("bg=") < 0 || document.cookie.indexOf("bg=animate") >= 0){
  bgcheck.checked = true;
  granimInstance = new Granim({
    element: '#canvas-basic',
    name: 'basic-gradient',
    direction: 'left-right', // 'diagonal', 'top-bottom', 'radial'
    opacity: [1, 1],
    isPausedWhenNotInView: true,
    states : {
        "default-state": {
            gradients: [
                ['#122036', '#043a61'],
                ['#02AAB0', '#00CDAC'],
                ['#22ff8f', '#e5ff7e'],
                ['#fff022', '#ee7133'],
                ['#31e97b', '#626262'],
                ['#ffca6f', '#ff4545'],
                ['#45d09a', '#740000'],
            ],
            transitionSpeed: 9500
        }
    }
  });
} else {
  bgcheck.checked = false;
  // alert('it"s off i guess')
}


function bfullbackground(e){
    if (bgcheck.checked) {
      var cf = confirm("Deseja desactivar o fundo animado?");
      if (cf == true) {
        granimInstance.clear();
        bgcheck.checked = false;
        document.cookie = "bg=dontanimate; expires=Thu, 18 Dec 2020 12:00:00 UTC; path=/";
        window.location.reload(1);

      } else {
        return null;
      }

    } else {
      var cf = confirm("Deseja activar o fundo animado?");
      if (cf ==true) {
        bgcheck.checked = true;
        document.cookie = "bg=animate; expires=Thu, 18 Dec 2020 12:00:00 UTC; path=/";
        window.location.reload(1);

      } else {
        return null;
      }
    }
    console.log(e);
  }



  function animatep(element){
      if (element._side == "upp") {
          element._side = "downn";

          transition.begin(element, [
            ["transform", "translateX(0) translateY(0)", "1s", 'ease-in-out'],
            ["width", "0", "70px", "200ms"],
            ["height", "0", "70px", "200ms"],
          ], {
            beginFromCurrentValue: true,
            onBeforeChangeStyle: function(element) {
                element.classList = "button button--material start_button";
                element.innerHTML = '<strong>></strong>';

                transition.begin(document.getElementById('sobre_anunciosbutton'), [
                  ["opacity", "0", "1", "linear", "50ms"],

                ], {
                  onBeforeChangeStyle: function(){
                    document.getElementById('app_second').setAttribute("hidden", "");
                    document.getElementById('sobre_anunciosbutton').removeAttribute("hidden", "");
                    document.getElementById('olatext').removeAttribute("hidden", "");
                  }
                });

                transition.begin(document.getElementById('olatext'), [
                  ["opacity", "0", "1", "1-0ms"],

                ]);
            }
          });

      } else {
          // Next-Back button: From down go up
          element._side = "upp";
          transition.begin(element, [
            ["transform", "translateX(-72px) translateY(-57px)", "1s"],
            // ["background-color", "#444444", "200ms", "linear"],
            ["width", "0", "40px", "200ms"],
            ["height", "0", "25px", "200ms"],

          ], {
              beginFromCurrentValue: true,
              onAfterChangeStyle: function(element, finished) {
                  element.classList = "mdl-button mdl-js-button mdl-button--raised bb";
                  element.innerHTML = '<span style="padding: 0px;"><img style="margin-bottom: 12px;" width="13px" src="images/left_arrow.png"></span>';

                  transition.begin(document.getElementById('sobre_anunciosbutton'), [
                    ["opacity", "1", "0", "linear", "200ms"],

                  ], {
                    onBeforeChangeStyle: function(){
                      transition.begin(document.getElementById('app_second'), [
                        ["opacity", "0", "1", "linear", "200ms"],
                        // ["display", "block", "200ms"],
                      ], {
                        onAfterChangeStyle: function(){
                          document.getElementById('app_second').removeAttribute("hidden", "");
                          document.getElementById('sobre_anunciosbutton').setAttribute("hidden", "");
                          document.getElementById('olatext').setAttribute("hidden", "");
                        }
                      });
                    }
                  });

                  transition.begin(document.getElementById('olatext'), [
                    // ["display", "none", "200ms"],
                    ["opacity", "1", "0", "300ms"],
                  ]);
              }
          });
      }
    }







  //./////////////////////////////.//

    function animateESE(element){
      if (element._side == "up") {
          element._side = "down";
          transition.begin(element, [
            ["transform", "translateX(0) translateY(0)", "1s", "ease-in-out"],
            ["background-color", "#ffffff", "200ms", "linear"],
            ["width", "0", "80%", "80ms"],
            ["height", "0", "44px", "200ms"],

          ], {
              beginFromCurrentValue: true,
              onAfterChangeStyle: function(element, finished) {
                element.classList = "button button--material ensinoSecundario_button";
                element.innerHTML = '<strong>Ensino Secundário</strong>';

                document.getElementById('start_').removeAttribute("hidden", "");
                document.getElementById('oqueprocuras').removeAttribute("hidden", "");
                // document.getElementById('meusExames_button').removeAttribute("hidden", "");
                document.getElementById('ensinoSuperior_button').removeAttribute("hidden", "");
                document.getElementById('app_secundario').setAttribute("hidden", "");
              }
          });

      } else {
        // From down go up
          element._side = "up";
          transition.begin(element, [
            // ["transform", "translateX(0) translateY(-20px)", "1s", "ease-in-out"],
            ["background-color", "#ffffff", "200ms", "linear"],
            ["width", "0", "40px", "500ms"],
            ["height", "0", "25px", "200ms"],

          ], {
              beginFromCurrentValue: true,
              onBeforeChangeStyle: function(element, finished) {
                element.classList = "mdl-button mdl-js-button mdl-button--raised bb";
                element.innerHTML = '<span style="padding: 0px;"><img style="margin-bottom: 12px;" width="13px" src="images/left_arrow_black.png"></span>';
                document.getElementById('start_').setAttribute("hidden", "");
                document.getElementById('oqueprocuras').setAttribute("hidden", "");
                // document.getElementById('meusExames_button').setAttribute("hidden", "");
                document.getElementById('ensinoSuperior_button').setAttribute("hidden", "");
                document.getElementById('app_secundario').removeAttribute("hidden", "");
                transition.begin(document.getElementById('app_secundario'), [
                  // ["display", "none", "200ms"],
                  ["opacity", "0", "1", "400ms"],
                  ["transform", "translateY(-20px)", "translateY(0)", "600ms"],
                ]);
              }
          });
      }
    }



    /////////////////////

    function animateSOBRE(element){
      if (element._side == "up") {
          element._side = "down";
          transition.begin(element, [
            ["transform", "translateX(0) translateY(0)", "1s", "linear"],
            // ["background-color", "0", "#ffffff", "200ms", "linear"]
          ], {
              beginFromCurrentValue: true,
              onAfterChangeStyle: function(element, finished) {
                element.style = "padding: 0px 8px 0px";
                element.classList = "button button--material sobreremoveranuncios_button";
                element.innerHTML = "<strong>sobre</strong>";
                element.style.backgroundColor= "#ffffff";

                document.getElementById('start_').removeAttribute("hidden", "");
                document.getElementById('remad_').removeAttribute("hidden", "");

                transition.begin(document.getElementById('sobre_anunciosbutton'), [
                  ["opacity", "0", "1", "linear", "50ms"],
                ], {
                  onBeforeChangeStyle: function(){
                    document.getElementById('app_second').setAttribute("hidden", "");
                    document.getElementById('sobre_anunciosbutton').removeAttribute("hidden", "");
                    document.getElementById('olatext').removeAttribute("hidden", "");
                  }
                });

                transition.begin(document.getElementById('sobre_content'), [
                  // ["display", "none", "200ms"],
                  ["opacity", "1", "0", "200ms"],
                  ["transform", "translateY(-5px)", "translateY(0)", "600ms"],
                ], {
                  onBeforeChangeStyle: function(){
                    transition.begin(document.getElementById('body'), [
                      ["background-color", "#323232", "#fb4343", "500ms", "linear"],
                      // ["width", "0", "40px", "500ms"],
                      // ["height", "0", "25px", "200ms"],

                    ], {
                      onAfterChangeStyle: function(){
                        document.getElementById('body').classList.add('grad');
                      }
                    });
                    document.getElementById('sobre_content').setAttribute('hidden', '');
                  }
                });
              }
          });

      } else {
        // From down go up
          element._side = "up";
          transition.begin(element, [
            // ["transform", "translateX(0) translateY(0)", "1s", "linear"],
            ["width", "0", "40px", "500ms"],
            ["height", "0", "25px", "200ms"],

          ], {
              beginFromCurrentValue: true,
              onBeforeChangeStyle: function() {
                transition.begin(document.getElementById('body'), [
                  ["background-color", "#fb4343", "#323232", "500ms", "linear"],
                  // ["width", "0", "40px", "500ms"],
                  // ["height", "0", "25px", "200ms"],

                ], {
                  onBeforeChangeStyle: function(element){
                    element.classList.remove('grad');
                  }
                });
                element.style.backgroundColor= "#ffffff";
                element.classList = "mdl-button mdl-js-button mdl-button--raised bb";
                element.innerHTML = '<span style="padding: 0px;"><img style="margin-bottom: 12px;" width="13px" src="images/left_arrow_black.png"></span>';
                document.getElementById('start_').setAttribute("hidden", "");
                document.getElementById('remad_').setAttribute("hidden", "");
                document.getElementById('olatext').setAttribute("hidden", "");

                transition.begin(document.getElementById('sobre_content'), [
                  // ["display", "none", "200ms"],
                  ["opacity", "0", "1", "400ms"],
                  ["transform", "translateY(-5px)", "translateY(0)", "600ms"],
                ], {
                  onBeforeChangeStyle: function(){
                    document.getElementById('sobre_content').removeAttribute('hidden', '');
                  }
                });
              }
          });
      }
    }





  ///////////////////  //
    function animateESU(element){
      if (element._side == "up") {
          element._side = "down";
          transition.begin(element, [
            ["transform", "translateX(0) translateY(0)", "1s", "linear"],
            ["background-color", "#ffffff", "200ms", "linear"],
            ["width", "0", "80%", "80ms"],
            ["height", "0", "44px", "200ms"],
          ], {
              beginFromCurrentValue: true,
              onAfterChangeStyle: function(element, finished) {
                element.classList = "button button--material ensinoSuperior_button";
                element.innerHTML = '<strong>Ensino Superior</strong>';

                document.getElementById('start_').removeAttribute("hidden", "");
                document.getElementById('oqueprocuras').removeAttribute("hidden", "");
                // document.getElementById('meusExames_button').removeAttribute("hidden", "");
                document.getElementById('ensinoSecundario_button').removeAttribute("hidden", "");
                transition.begin(document.getElementById('app_superior'), [
                  // ["display", "none", "200ms"],
                  ["opacity", "1", "0", "400ms"],
                ]);
                document.getElementById('app_superior').setAttribute("hidden", "");
              }
          });

      } else {
        // From down go up
          element._side = "up";
          transition.begin(element, [
            // ["transform", "translateX(0) translateY(-20px)", "10s", "linear"],
            ["background-color", "#ffffff", "500ms", "linear"],
            ["width", "0", "40px", "500ms"],
            ["height", "0", "25px", "200ms"],
          ], {
              beginFromCurrentValue: true,
              onBeforeChangeStyle: function(element, finished) {
                element.classList = "mdl-button mdl-js-button mdl-button--raised bb";
                element.innerHTML = '<span style="padding: 0px;"><img style="margin-bottom: 12px;" width="13px" src="images/left_arrow_black.png"></span>';

                document.getElementById('start_').setAttribute("hidden", "");
                document.getElementById('oqueprocuras').setAttribute("hidden", "");
                // document.getElementById('meusExames_button').setAttribute("hidden", "");
                document.getElementById('ensinoSecundario_button').setAttribute("hidden", "");
                document.getElementById('app_superior').removeAttribute("hidden", "");

                transition.begin(document.getElementById('app_superior'), [
                  // ["display", "none", "200ms"],
                  ["opacity", "0", "1", "400ms"],
                  ["transform", "translateY(-20px)", "translateY(0)", "600ms"],
                ]);
              }
          });
      }
    }


      //ENSINO SECUNDÁRIO
      var classeObject = {
        // 10th Grade
        "10a Classe": {
          "Portugues": [
              "2000",
              "2001",
              "2002",
              "2003",
              "2009",
              "2011",
              "2012",
              "2013",
              "2014",
              "2015",
              "2016",
              "2017",
          ],

          "Matematica": [
              "2000",
              "2001",
              "2002",
              "2003",
              "2005",
              "2009",
              "2011",
              "2012",
              "2013",
              "2014",
              "2015",
              "2016",
              "2017"
          ],

          "Geografia": [
              "2000",
              "2002",
              "2003",
              "2005",
              "2009",
              "2011",
              "2012",
              "2013",
              "2014",
              "2016"
          ],

          "Biologia": [
              "2005",
              "2006",
              "2007",
              "2008",
              "2009",
              "2010",
              "2011",
              "2012",
              "2013",
              "2014",
              "2017"
          ],

          "Historia": [
              "2000",
              "2001",
              "2002",
              "2003",
              "2006",
              "2007",
              "2008",
              "2009",
              "2010",
              "2011",
              "2012",
              "2013",
              "2014",
              "2015",
              "2017"
          ],

          "Ingles": [
              "2005",
              "2006",
              "2007",
              "2008",
              "2009",
              "2011",
              "2012",
              "2013",
              "2014",
              "2015",
              "2016",
              "2017"
          ],

          "Fisica": [
              "1995",
              "1997",
              "2000",
              "2001",
              "2002",
              "2003",
              "2004",
              "2005",
              "2006",
              "2007",
              "2008",
              "2009",
              "2010",
              "2011",
              "2012",
              "2013",
              "2014"
          ],

          "Quimica": [
              "2009",
              "2011",
              "2012",
              "2013",
              "2014",
              "2016"
          ]
        },

        // 12th Grade
        "12a Classe": {
          "Biologia": [
            "2000",
            "2001",
            "2002",
            "2003",
            "2009",
            "2011",
            "2012",
            "2013",
            "2014",
            "2016"
          ],

          "Desenho": [
            "2000",
            "2001",
            "2002",
            "2003",
            "2011",
            "2012",
            "2013",
            "2014",
            "2017"
          ],

          "Filosofia": [
            "2002",
            "2003",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2017"
          ],

          "Fisica": [
            "2000",
            "2001",
            "2002",
            "2003",
            "2011",
            "2012",
            "2013",
            "2014",
            "2016"
          ],

          "Frances": [
            "2000",
            "2001",
            "2002",
            "2003",
            "2011",
            "2012",
            "2013",
            "2014"
          ],

          "Geografia": [
            "2009",
            "2010",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2016",
            "2017"
          ],

          "Historia": [
            "2008",
            "2011",
            "2012",
            "2013",
            "2014"
          ],

          "Ingles": [
            "2000",
            "2002",
            "2003",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2017"
          ],

          "Matematica": [
            "2008",
            "2009",
            "2010",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2016",
            "2017"
          ],

          "Portugues": [
            "2000",
            "2001",
            "2002",
            "2003",
            "2008",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2016",
            "2017"
          ],

          "Quimica": [
            "2000",
            "2001",
            "2002",
            "2003",
            "2011",
            "2012",
            "2013",
            "2014",
            "2017"
          ],

        }
      };
      // ENSINO SUPERIOR
      var universidadeObject = {
        // Universidade Pedagogica
        "Universidade Pedagógica (UP)": {
            "Biologia": [
                "2007",
                "2008",
                "2009",
                "2010",
                "2011",
                "2014",
                "2016",
                "2017"
            ],

            "Biologia para Educacao Fisica": [
                "2010",
                "2011"
            ],

            "Filosofia": [
                "2010",
                "2013",
                "2014",
                "2016",
                "2017"
            ],

            "Fisica": [
                "2004",
                "2008",
                "2010",
                "2011",
                "2013",
                "2014",
                "2016",
                "2017"
            ],

            "Frances": [
                "2008",
                "2016",
                "2017"
            ],

            "Geografia": [
                "2013",
                "2014",
                "2016",
                "2017"
            ],

            "Historia": [
                "2009",
                "2010",
                "2016",
                "2017"
            ],

            "Ingles": [
                "2010",
                "2013",
                "2016",
                "2017"
            ],

            "Matematica": [
                "2010",
                "2011",
                "2014",
                "2016",
                "2017"
            ],

            "Portugues": [
                "2009",
                "2010",
                "2011",
                "2012",
                "2013",
                "2014",
                "2016",
                "2017"
            ],

            "Quimica": [
                "2007",
                "2008",
                "2009",
                "2013",
                "2016",
                "2017"
            ],
        },

        // Universidade Eduardo Mondlane
        "Universidade Eduardo Mondlane (UEM)": {
            "Biologia": [
              "2004",
              "2005",
              "2007",
              "2008",
              "2009",
              "2010",
              "2011",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Desenho 1":[
              "2005",
              "2007",
              "2009",
              "2010",
              "2011",
              "2012",
              "2013",
              "2014",
              "2017"
            ],

            "Desenho 2":[
              "2009",
              "2010"
            ],

            "Filosofia": [
              "2011",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Fisica": [
              "2004",
              "2005",
              "2006",
              "2007",
              "2008",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Frances": [
              "2004",
              "2005",
              "2007",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Geografia": [
              "2004",
              "2005",
              "2006",
              "2007",
              "2008",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Historia": [
              "2005",
              "2006",
              "2007",
              "2008",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Ingles": [
              "2004",
              "2005",
              "2006",
              "2007",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Matematica": [
              "2004",
              "2005",
              "2007",
              "2008",
              "2009",
              "2010",
              "2011",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Musica": [
              "2013",
              "2016"
            ],

            "Portugues 1": [
              "2005",
              "2007",
              "2009",
              "2010",
              "2011",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Portugues 2": [
              "2010",
              "2011",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Quimica": [
              "2004",
              "2005",
              "2006",
              "2007",
              "2008",
              "2013",
              "2014",
              "2016",
              "2017"
            ],

            "Teatro": [
              "2014",
              "2016"
            ],
          },

        // Instituto Superior De Ciências de Saúde
        "Instituto Superior de Ciências de Saúde (ISCISA)": {
              "Biologia": [
                "2015"
              ],

              "Historia": [
                "2015"
              ],

              "Matematica": [
                "2015"
              ],

              "Portugues": [
                "2015"
              ],

              "Quimica": [
                "2015"
              ],
        },

        // Instituto Superior de Relações Internacionais
        "Instituto Superior de Relações Internacionais (ISRI)": {
              "Geografia": [
                  "2007",
                  "2010",
                  "2013",
                  "2015"
              ],

              "Historia": [
                "2007",
                "2011"
              ],

              "Portugues": [
                "2013",
                "2015"
              ],
        }
      };
